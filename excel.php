<?php 
    require 'conexion/conexion.php';
    require 'Classes/PHPExcel.php';
    
        //Consulta para el llenado de excel
        $sql2 = "SELECT* from actividad_realizar";
        $rec = $mysqli->query($sql2);
    
        $fila = 2;
    
        $objPHPExcel = new PHPExcel();
    
        //Propiedades del archivo excel
        $objPHPExcel->getProperties()->setCreator("Hugo Lara")
        ->setDescription("Reporte");
        
        $objPHPExcel->setActiveSheetIndex(0);
    
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Reported de Actividades');
    
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Área');
        $objPHPExcel->getActiveSheet()->setCellValue('B1','Jefatura de área');
        $objPHPExcel->getActiveSheet()->setCellValue('C1','Actividad');
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Responsable');
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Fecha del reportes');
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Fecha de inicio');
        $objPHPExcel->getActiveSheet()->setCellValue('G1','Fecha de termino');
        $objPHPExcel->getActiveSheet()->setCellValue('H1','Observaciones');
    
        //Llenado de tablas
        while($rowAC=$rec->fetch_assoc()){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila,$rowAC['Nombre_Area']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila,$rowAC['Nombre_Jefe']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila,$rowAC['Nombre_Actividad']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila,$rowAC['Nombre_Empleado']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila,$rowAC['FechaReport']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila,$rowAC['Fecha_1']);        
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila,$rowAC['Fecha_2']);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila,$rowAC['Observaciones']);
    
            $fila++;
            
        }
    
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Reporte de Actividades.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=0');
    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('php://output');
    
?>