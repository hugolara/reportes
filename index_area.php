<?php
	require 'conexion/conexion.php';
	
	$where = "";
	
	if(!empty($_POST))
	{
		$valor = $_POST['campo'];
		if(!empty($valor)){
			$where = "WHERE Nombre_Area LIKE '%$valor'";
		}
	}
	$sql = "SELECT * FROM areas $where ORDER BY Id_Area";
	$resultado = $mysqli->query($sql);
	
?>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-theme.css" rel="stylesheet">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
	</head>
	
	<body>
		
		<div class="container">
			<div class="row">
				<h2 style="text-align:center">Departamentos</h2>
			</div>
			
			<div class="row">
				<a href="nueva_area.php" class="btn btn-primary">Nuevo Registro</a>
				<!--a href="excel.php" class="btn btn-primary">Descargar Reporte</a-->
				
				<form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
					<b>Departamento: </b><input type="text" id="campo" name="campo" />
					<input type="submit" id="enviar" name="enviar" value="Buscar" class="btn btn-info" />
				</form>
			</div>
			
			<br>
			
			<div class="row table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Código del Departamento</th>
							<th>Departamento</th>
							<th></th>
							<!--th></th-->
						</tr>
					</thead>
					
					<tbody>
						<?php while($row = $resultado->fetch_array(MYSQLI_ASSOC)) { ?>
							<tr>
								<td><?php echo $row['Id_Area']; ?></td>
								<td><?php echo $row['Nombre_Area']; ?></td>
								<td><a href="modificar_area.php?id=<?php echo $row['Id_Area']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
								<!--td><a href="#" data-href="eliminar_jefe.php?id=<//?php echo $row['Id_Jefe']; ?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></a></td-->
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<a href="index.php" class="btn btn-primary">Regresar</a>
			</div>
		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
					</div>
					
					<div class="modal-body">
						¿Desea eliminar este registro?
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a class="btn btn-danger btn-ok">Eliminar</a>
					</div>
				</div>
			</div>
		</div>
		
		<script>
			$('#confirm-delete').on('show.bs.modal', function(e) {
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				
				$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
			});
		</script>
			
		
	</body>
</html>	