<?php
    //Llamado del archivo conexion y phpexcel
    require 'conexion/conexion.php';

	$id=$_GET['id'];

    // Consulta para el primer combobox
    $sql1 = "SELECT Nombre_Area from areas";
	$result1 = $mysqli->query($sql1);
	
	$sql = "SELECT* FROM actividad_realizar where Id ='$id'";
	$resultado = $mysqli->query($sql);
	$row = $resultado->fetch_array(MYSQLI_ASSOC);

?>

<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-theme.css" rel="stylesheet">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script language="javascript">
        //Codigo Para el llenado de actividades
			$(document).ready(function(){
				$("#dept").change(function () {

					//$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#dept option:selected").each(function () {
						Nombre_Area = $(this).val();
						$.post("includes/getActividad.php", { Nombre_Area: Nombre_Area}, function(data){
							$("#actividades").html(data);
						});            
					});
				})
			});
			
            //Codigo para el llenado de Empleados
			$(document).ready(function(){
				$("#dept").change(function () {

					//$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#dept option:selected").each(function () {
						Nombre_Area = $(this).val();
						$.post("includes/getEmpleado.php", { Nombre_Area: Nombre_Area}, function(data){
							$("#responsable").html(data);
						});            
					});
				})
			});
			
            //Codigo para el llenado de Empleados
			$(document).ready(function(){
				$("#dept").change(function () {

					//$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#dept option:selected").each(function () {
						Nombre_Area = $(this).val();
						$.post("includes/getJefe.php", { Nombre_Area: Nombre_Area}, function(data){
							$("#jefe").html(data);
						});            
					});
				})
			});
		</script>	
	</head>
	
	<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">MODIFICAR REGISTRO</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="update.php" autocomplete="off">
				<div class="form-group">
					<label for="dept" class="col-sm-2 control-label">Departamento</label>
					<div class="col-sm-10">
						<input type="hidden" id="id" name="id" value="<?php echo $id?>">
						<select class="form-control" id="dept" name="dept">
							<option value="0">Seleccionar departamento</option>
							<?php while($row = $result1->fetch_assoc()){ ?>
								<option value="<?php echo $row['Nombre_Area']; ?>"><?php echo $row['Nombre_Area']; ?></option>
							<?php }?>				
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="jefe" class="col-sm-2 control-label">Jefatura del Departamento</label>
					<div class="col-sm-10">
						<select class="form-control" id="jefe" name="jefe">				
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="actividades" class="col-sm-2 control-label">Actividades</label>
					<div class="col-sm-10">
						<select class="form-control" id="actividades" name="actividades">				
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="responsable" class="col-sm-2 control-label">Responsable</label>
					<div class="col-sm-10">
						<select class="form-control" id="responsable" name="responsable">				
						</select>
					</div>
				</div>
				
				<!--div class="form-group">
					<label for="fechareport" class="col-sm-2 control-label">Fecha del Report</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="fechareport" name="fechareport" value="<?php echo date("Y-m-d"); ?>" 
            			required min=<//?php $hoy=date("Y-m-d"); echo $hoy;?>>
					</div>
				</div-->
				
				<div class="form-group">
					<label for="fecha1" class="col-sm-2 control-label">Fecha de Inicio</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="fecha1" name="fecha1" value="<?php echo date("Y-m-d"); ?>" 
						required min=<?php $hoy=date("Y-m-d"); echo $hoy;?>>
					</div>
				</div>

				<div class="form-group">
					<label for="fecha2" class="col-sm-2 control-label">Fecha de Termino</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="fecha2" name="fecha2" value="<?php echo date("Y-m-d"); ?>" 
						required min=<?php $hoy=date("Y-m-d"); echo $hoy;?>>
					</div>
				</div>

				<!--div class="form-group">
					<label for="observaciones" class="col-sm-2 control-label">Comentarios</label>
					<div class="col-sm-10">
						<input type="text"  class="form-control" name="observaciones" id="observaciones" placeholder="Comentarios">
					</div>
				</div-->
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="index.php" class="btn btn-default">Regresar</a>
						<button type="submit" class="btn btn-primary">Modificar</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>