<?php
    //Llamado del archivo conexion y phpexcel
	require 'conexion/conexion.php';
	
	$id=$_GET['id'];



    // Consulta para el primer combobox
    $sql1 = "SELECT Nombre_Area from areas";
	$result1 = $mysqli->query($sql1);

	$sql = "SELECT * FROM empleado WHERE Id_Empleado = '$id'";
	$resultado = $mysqli->query($sql);
	$row = $resultado->fetch_array(MYSQLI_ASSOC);


?>

<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-theme.css" rel="stylesheet">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!--script language="javascript">
        //Codigo Para el llenado de actividades
			$(document).ready(function(){
				$("#dept").change(function () {

					//$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#dept option:selected").each(function () {
						Nombre_Area = $(this).val();
						$.post("includes/getActividad.php", { Nombre_Area: Nombre_Area}, function(data){
							$("#actividades").html(data);
						});            
					});
				})
			});
			
            //Codigo para el llenado de Empleados
			$(document).ready(function(){
				$("#dept").change(function () {

					//$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#dept option:selected").each(function () {
						Nombre_Area = $(this).val();
						$.post("includes/getEmpleado.php", { Nombre_Area: Nombre_Area}, function(data){
							$("#responsable").html(data);
						});            
					});
				})
			});
			
            //Codigo para el llenado de Empleados
			$(document).ready(function(){
				$("#dept").change(function () {

					//$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#dept option:selected").each(function () {
						Nombre_Area = $(this).val();
						$.post("includes/getJefe.php", { Nombre_Area: Nombre_Area}, function(data){
							$("#jefe").html(data);
						});            
					});
				})
			});
		</script-->	
	</head>
	
	<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">MODIFICAR REGISTRO</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="update_empleado.php" autocomplete="off">
			<div class="form-group">
					<label for="nombre" class="col-sm-2 control-label">Nombre</label>
					<div class="col-sm-10">
						<input type="hidden" id="id" name="id" value="<?php echo $id?>" >
						<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $row['Nombre_Empleado']?>" required>
					</div>
				</div>
				
				<div class="form-group">
					<label for="dept" class="col-sm-2 control-label">Departamento</label>
					<div class="col-sm-10">
						<select class="form-control" id="dept" name="dept">
							<option value="0">Seleccionar departamento</option>
							<?php while($row = $result1->fetch_assoc()){ ?>
								<option value="<?php echo $row['Nombre_Area']; ?>"><?php echo $row['Nombre_Area']; ?></option>
							<?php }?>				
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="telefono" class="col-sm-2 control-label">Teléfono</label>
					<div class="col-sm-10">
						<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Telefono" value="<?php echo $row['Telefono']?>" >
					</div>
				</div>
				
				<div class="form-group">
					<label for="telefono" class="col-sm-2 control-label">Dirección</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="<?php echo $row['Direccion'] ?>" >
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="index_empleado.php" class="btn btn-default">Regresar</a>
						<button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>