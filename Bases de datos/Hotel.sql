create database hotel;

use hotel;

//Creacion de las tablas

create table Areas(
    Id_Area int NOT NULL AUTO_INCREMENT,
    Nombre_Area varchar(50) not null,
    constraint pk_areas primary key(Id_Area)
);

create table Jefe_Area(
    Id_Jefe int NOT NULL AUTO_INCREMENT,
    Nombre_Jefe varchar(50) not null,
    Nombre_Area varchar(50) not null,
    Telefono varchar(30),
    Direccion varchar(30),
    constraint pk_jefe primary key(Id_Jefe,Nombre_Jefe)
);

create table Empleado(
    Id_Empleado int NOT NULL AUTO_INCREMENT,
    Nombre_Empleado varchar(50) not null,
    Nombre_Area varchar(50) not null,
    Telefono varchar(30),
    Direccion varchar(30),
    constraint pk_empleado primary key(Id_Empleado, Nombre_Empleado)
);

create table Actividad(
    Id_Actividad int NOT NULL AUTO_INCREMENT,
    Nombre_Actividad varchar(50) not null,
    Nombre_Area varchar(50) not null,
    constraint pk_actividad primary key(Id_Actividad,Nombre_Actividad)
);

create table Actividad_Realizar(
	Id bigint NOT NULL AUTO_INCREMENT,
    Nombre_Area varchar(50) not null,
    Nombre_Jefe varchar(50) not null,
    Nombre_Actividad varchar(50) not null,
    Nombre_Empleado varchar(50) not null,
    FechaReport date not null,
    Fecha_1 date not null,
    Fecha_2 date not null,
    Observaciones varchar(250) not null,
    Antes longblob,
    Despues longblob,
    constraint pk_Act_Real primary key(Id)
);

//Llaves foraneas

alter table Jefe_Area
add constraint fk_jefe
foreign key (Nombre_Area)
references Areas(Nombre_Area);

alter table Empleado
add constraint fk_empleado
foreign key (Nombre_Area)
references Areas(Nombre_Area);

alter table Actividad
add constraint fk_actividad
foreign key (Nombre_Area)
references Areas(Nombre_Area);

alter table Actividad_Realizar
add constraint fk_Act_rel1
foreign key (Nombre_Area)
references Areas(Nombre_Area);

alter table Actividad_Realizar
add constraint fk_Act_rel2
foreign key (Nombre_Jefe)
references Jefe_Area(Nombre_Jefe);

alter table Actividad_Realizar
add constraint fk_Act_rel3
foreign key (Nombre_Actividad)
references Actividad(Nombre_Actividad);

alter table Actividad_Realizar
add constraint fk_Act_rel4
foreign key (Nombre_Empleado)
references Empleado(Nombre_Empleado);

//Datos En Tablas

insert into Areas (Nombre_Area)
values('Mantenimiento'),('Lavanderia'),('Sistemas'),
('Recursos humanos');

insert into Jefe_Area (Nombre_Jefe,Nombre_Area)
values ('Franco de la Cruz','Mantenimiento'),('Ricardo Ortiz','Lavanderia'),
('Hugo Lara','Sistemas'),('Karen Ruiz','Recursos humanos');

insert into Empleado (Nombre_Empleado, Nombre_Area)
values 
('Santiago Palacio','Mantenimiento'), ('Domingo Fernandez','Mantenimiento'),('Jose Amado','Mantenimiento'),
('Carla Mugica','Lavanderia'),('Rosa Aranguren','Lavanderia'),('Jaime Barba','Lavanderia'),
('Andres Portero','Sistemas'),('Santiago Venegas','Sistemas'),('Emilio Fajardo','Sistemas'),
('Milagros Bernand','Recursos humanos'),('Eva Parra','Recursos humanos'),('Maria Giner','Recursos humanos');

insert into Actividad (Nombre_Actividad,Nombre_Area)
values
('Revisar instalacion electrica','Mantenimiento'),('Reparar A/c','Mantenimiento'),('Cambiar focos','Mantenimiento'),
('Lavar sabanas','Lavanderia'),('Lavar toallas','Lavanderia'),('Lavar almohadas','Lavanderia'),
('Cambiar claves de acceso','Sistemas'),('Mantenimiento a equipos','Sistemas'),('Copia de seguridad','Sistemas'),
('Pagos a empleados','Recursos humanos'),('Facturas','Recursos humanos'),('Depedir a','Recursos humanos');