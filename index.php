<?php
	require 'conexion/conexion.php';
	
	$where = "";
	
	if(!empty($_POST))
	{
		$valor = $_POST['campo'];
		if(!empty($valor)){
			$where = "WHERE Nombre_Empleado LIKE '%$valor'";
		}
		//$valor = $_POST['campo'];
		/*if(!empty($valor)){
			$where = "WHERE Nombre_Area LIKE '%$valor'";
		}
		//$valor = $_POST['campo'];
		if(!empty($valor)){
			$where = "WHERE Nombre_Actividad LIKE '%$valor'";
		}*/
	}

	/*if(!empty($_POST))
	{
		$valor1 = $_POST['campo2'];
		if(!empty($valor1)){
			$where = "WHERE Nombre_Area LIKE '%$valor1'";
		}
	}

	if(!empty($_POST))
	{
		$valor2 = $_POST['campo3'];
		if(!empty($valor2)){
			$where = "WHERE Nombre_Actividad LIKE '%$valor2'";
		}
	}*/
	$sql = "SELECT * FROM actividad_realizar $where ORDER BY Id";
	$resultado = $mysqli->query($sql);

	 // Consulta para el primer combobox
	 $sql1 = "SELECT Nombre_Empleado from empleado";
	 $result1 = $mysqli->query($sql1);

	 $sql2 = "SELECT Nombre_Area from areas";
	 $result2 = $mysqli->query($sql2);
	
?>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-theme.css" rel="stylesheet">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
	</head>
	
	<body>
		
		<div class="container">
			<div class="row">
				<h2 style="text-align:center">Registros de las actividades</h2>
			</div>
			
			<div class="row">
				<a href="nuevo.php" class="btn btn-primary">Registrar</a>
				<a href="excel.php" class="btn btn-primary">Reporte</a>
				<a href="index_actividad.php" class="btn btn-primary">Actividades</a>
				<a href="index_empleado.php" class="btn btn-primary">Empleados</a>
				<a href="index_area.php" class="btn btn-primary">Departamentos</a>
				<a href="index_jefe.php" class="btn btn-primary">Jefaturas</a>
				
				<form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
					<b>Nombre Responsable: </b><input type="text" id="campo" name="campo" />
					<input type="submit" id="enviar1" name="enviar1" value="Buscar" class="btn btn-info" />
				</form>
				<!--form action="<//?php $_SERVER['PHP_SELF']; ?>" method="POST">
					<b>Departamento: </b><input type="text" id="campo2" name="campo2" />
					<input type="submit" id="enviar2" name="enviar2" value="Buscar" class="btn btn-info" />
				</form>
				<form action="<//?php $_SERVER['PHP_SELF']; ?>" method="POST">
					<b>Actividad: </b><input type="text" id="campo3" name="campo3" />
					<input type="submit" id="enviar3" name="enviar3" value="Buscar" class="btn btn-info" />
				</form-->
			</div>
			
			<br>
			
			<div class="row table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Código de actividad</th>
							<th>Departamento</th>
							<th>Jefatura del Departamento</th>
							<th>Actividad</th>
							<th>Responsable</th>
							<th>Fecha del reporte</th>
							<th>Fecha de Inicio</th>
							<th>Fecha de Termino</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					
					<tbody>
						<?php while($row = $resultado->fetch_array(MYSQLI_ASSOC)) { ?>
							<tr bgcolor=<?php
								$fecha =  Date("Y-m-d");
								$fecha2 = $row['Fecha_2'];
								$fondo;
							
								if($fecha > $fecha2){
								$fondo = "#c42a09";
								}
								elseif($fecha < $fecha2){
								$fondo = "09c411";
								}elseif($fecha = $fecha2){
								$fondo = "#d6dc07";
								}
								echo $fondo?> >
								<td><?php echo $row['Id']; ?></td>
								<td><?php echo $row['Nombre_Area']; ?></td>
								<td><?php echo $row['Nombre_Jefe']; ?></td>
								<td><?php echo $row['Nombre_Actividad']; ?></td>
								<td><?php echo $row['Nombre_Empleado']; ?></td>
								<td><?php echo $row['FechaReport']; ?></td>
								<td><?php echo $row['Fecha_1']; ?></td>
								<td><?php echo $row['Fecha_2']; ?></td>
								<td><a href="modificar.php?id=<?php echo $row['Id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
								<td><a href="#" data-href="eliminar.php?id=<?php echo $row['Id']; ?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<!--Reporete en Excel dependiendo del empleado-->
			<form action="excel_empleado.php" method="POST">
					<b>Nombre Responsable: </b><select class="form-control" id="emp" name="emp">
							<option value="0">Seleccionar responsable</option>
							<?php while($row = $result1->fetch_assoc()){ ?>
								<option value="<?php echo $row['Nombre_Empleado']; ?>"><?php echo $row['Nombre_Empleado']; ?></option>
							<?php }?>				
						</select>
					<input type="submit" id="env" name="env" value="Reporte" class="btn btn-info" />
			</form>
			<!--Reporte dependiendo del departamento-->
			<form action="excel_area.php" method="POST">
					<b>Nombre Departamento: </b><select class="form-control" id="area" name="area">
							<option value="0">Seleccionar departamento</option>
							<?php while($row = $result2->fetch_assoc()){ ?>
								<option value="<?php echo $row['Nombre_Area']; ?>"><?php echo $row['Nombre_Area']; ?></option>
							<?php }?>				
						</select>
					<input type="submit" id="env" name="env" value="Reporte" class="btn btn-info" />
			</form>
			<!--Reporte dependiendo de las fechas-->
			<form action="excel_fecha.php" method="POST">
					<b>Primer fecha:</b><input type="date" class="form-control" id="fecha1" name="fecha1" >
					<b>Segunda fecha:</b><input type="date" class="form-control" id="fecha2" name="fecha2">
					<input type="submit" id="env" name="env" value="Reporte" class="btn btn-info" />
			</form>
		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
					</div>
					
					<div class="modal-body">
						¿Desea eliminar este registro?
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a class="btn btn-danger btn-ok">Eliminar</a>
					</div>
				</div>
			</div>
		</div>
		
		<script>
			$('#confirm-delete').on('show.bs.modal', function(e) {
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				
				$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
			});
		</script>	
		
	</body>
</html>	